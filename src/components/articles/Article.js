import React, { Component } from 'react'
import { Text, View, FlatList, StyleSheet, Dimensions, Image, SafeAreaView, ActivityIndicator, TouchableOpacity, Alert } from 'react-native'
import { connect } from 'react-redux'
import { fetchArticle, deleteArticle } from './../../redux/actions/articleActions'

class Article extends Component {
    constructor(props) {
        super(props)
        this.state = {
            page: 1,
            isLoading: false,
            refreshing: false
        }
    }
    componentDidMount() {
        this.setState({ isLoading: true })
        this.props.fetchArticle(this.state.page)
    }
    renderFooter = () => {
        return this.state.isLoading && <ActivityIndicator size="large" />
    }
    handleLoadMore = () => {
        console.log("page", this.state.page)
        this.state.page >= this.props.totalPage ? this.setState({ isLoading: false }) :
            this.setState({ page: this.state.page + 1, isLoading: true })
        this.props.fetchArticle(this.state.page)
    }

    fetchNewData = () => {
        console.log("fetchnew")
        this.setState({page: 1})
        this.props.fetchArticle(this.state.page)
    }

    render() {
        console.log('article', this.props.articles)
        const { articles, deleteArticle } = this.props
        return (
            <SafeAreaView>
                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={articles}
                    renderItem={({ item, index }) =>
                        <TouchableOpacity
                            onLongPress={() => {
                                Alert.alert(
                                    "Are you sure?",
                                    "Delete Article",
                                    [
                                        {
                                            text: "Cancel",
                                            onPress: () => console.log("Cancel Pressed"),
                                            style: "cancel"
                                        },
                                        { text: "OK", onPress: () => deleteArticle(item.id) }
                                    ],
                                    { cancelable: false }
                                )
                            }
                            }
                        >
                            <Items
                                imageSource={item.image_url}
                                title={item.title}
                                desc={item.description}
                                createdDate={item.created_date}
                            />
                        </TouchableOpacity>
                    }
                    legacyImplementation={true}
                    onEndReachedThreshold={0}
                    refreshing={this.state.refreshing}
                    onRefresh={this.fetchNewData}
                    onEndReached={this.handleLoadMore}
                    ListFooterComponent={this.renderFooter}
                    keyExtractor={(item, index) => index.toString()}

                />
            </SafeAreaView>
        )
    }
}

/// MARK: connect to store and subscribe
const mapStateToProps = (state) => {
    return {
        articles: state.articleR.articles,
        totalPage: state.articleR.totalPage
    }
}

export default connect(mapStateToProps, { fetchArticle, deleteArticle })(Article)


export function Items({ imageSource, title, desc, createdDate }) {
    return (
        <View style={{ flex: 1, flexDirection: 'row', backgroundColor: 'whitesmoke', margin: 5 }}>
            <Image
                style={{ width: 110, height: 110 }}
                source={{ uri: imageSource == null ? 'https://images.pexels.com/photos/2236713/pexels-photo-2236713.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500' : imageSource }}
            />
            <View style={{ flex: 1, flexDirection: 'column' }}>
                <Text
                    numberOfLines={1}
                    ellipsizeMode="tail"
                    style={{ fontSize: 20, color: 'red', margin: 10 }}>{title}</Text>
                <Text
                    numberOfLines={2}
                    ellipsizeMode="tail"
                    style={{ color: 'darkgray', margin: 10 }}>{desc}</Text>
                <Text
                    numberOfLines={2}
                    ellipsizeMode="tail"
                    style={{ color: 'darkgray', margin: 10 }}>{createdDate}</Text>
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        margin: 10,
        flex: 1,
        justifyContent: 'center',

    },
    containerCard: {
        marginTop: 5

    },
    searchBarStyle: {
        width: Dimensions.get('screen').width - 20,
        height: 60,
        borderWidth: 1,
        borderColor: 'grey',
        borderRadius: 15,
        marginTop: -5,
        marginBottom: 15,
        padding: 10

    }
})