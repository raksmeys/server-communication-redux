import {baseURL, header} from './../../api/API'
import {GET_ARTICLE, DELETE_ARTICLE} from './articleActionTypes'
import axios from 'axios'

export const fetchArticle = (page) => {
    return async (dispatch) => {
        await axios(`${baseURL}articles?page=${page}&limit=15`,{
           method: 'GET',
           headers: header 
        })
        .then((res) => {
            console.log('action article', res.data.data)
            dispatch({
                type: GET_ARTICLE,
                articles: res.data.data,
                totalPage: res.data.pagination.total_pages
            })
        })
    }
}

export const deleteArticle = (id) => {
    return async (dispatch) => {
        await axios(`${baseURL}articles/${id}`,{
           method: 'DELETE',
           headers: header 
        })
        .then((res) => {
            dispatch({
                type: DELETE_ARTICLE,
                data: id,
            })
        })
    }
}