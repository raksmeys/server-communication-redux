import {combineReducers} from 'redux'
import { articleReducer } from './articleReducer'

const reducers = {
    articleR: articleReducer,

}

export const rootReducer = combineReducers(reducers)