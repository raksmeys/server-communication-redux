import {DELETE_ARTICLE, GET_ARTICLE} from './../actions/articleActionTypes'

const initializeState = {
    articles: [],
    totalPage: 1
}

export const articleReducer = (state = initializeState, action) => {
    switch (action.type){
        case GET_ARTICLE: 
            return {
                ... state,
                articles: state.articles.concat(action.articles),
                totalPage: action.totalPage
            }
        case DELETE_ARTICLE:
            return {
                ... state,
                articles: state.articles.filter(article => article.id !== action.data)
            }
        default:
            return state
    }
}