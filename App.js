import React, { Component } from 'react'
import { Text, View } from 'react-native'
import {Provider} from 'react-redux'
import Article from './src/components/articles/Article'
import { store } from './src/redux/store/centralStore'

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Article />
      </Provider>
    )
  }
}
